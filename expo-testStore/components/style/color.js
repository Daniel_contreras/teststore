export const color = {
    blue:'#5a7391',
    blueTrasn:'#5a7391aa',
    blueInative:'#adb9c9',
    while: '#fff',
    whileTwo: '#e9e9f5',
    whileTrasn: '#ffffff66',
    yellow: '#ffd33e',
}