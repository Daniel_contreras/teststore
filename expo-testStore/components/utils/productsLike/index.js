import React from 'react';
import { connect } from 'react-redux';
import { View, Text, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { button } from '../../style/button';
import { product } from '../../style/product';
import { productLike } from '../../style/productLike';
import { contenedor } from '../../style/contenedor';
import { color } from '../../style/color';

class productsLike extends React.Component {
    constructor(props){
        super(props)
    }
    render() {
        const { img, name, descrition, value, promo, icon, tintColor } = this.props;
        return(
            <View style={contenedor.productLike}>
                {promo ?<View style={product.contPromo}>
                    <Text style={product.textPromo}>HOT</Text>
                </View> : <Text/>}
                <Image style={productLike.img} source={{uri:img}} resizeMode={'cover'}  />
                <View style={productLike.cont}>
                    <Image style={productLike.icon} source={{uri:icon}} resizeMode={'cover'}  />
                    <View style={productLike.contInfo}>
                        <Text style={product.name} >{`${name.substr(0, 50)}${name.length > 50 ? '...' : '' }`}</Text>
                        <Text style={product.descrition} >{`${descrition.substr(0, 200)}${descrition.length > 200 ? '...' : '' }`}</Text>
                    </View>
                    <View style={productLike.contButton}>
                        <Text style={product.value}>{value}</Text>
                        <Text style={button.primary}>like</Text>
                    </View>
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
      };
  }
  
  const mapDispatchToProps = () => {
      return{  
      }
    };
  
  const ProductsLike = connect(
    mapStateToProps,
    mapDispatchToProps()
  )(productsLike)
  
  export default ProductsLike 