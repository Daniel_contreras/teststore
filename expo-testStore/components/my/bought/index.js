import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { connect } from 'react-redux';
import { View, ScrollView } from 'react-native';

import ProductsOption from '../../utils/productsOption';
import { contenedor } from '../../style/contenedor';


class brand extends React.Component {
    constructor(props){
        super(props)
        this.state = {
          productsOption: [
            {
              img:'https://images.rtg-prod.com/accent-cabinets_room-grid.jpg?cache-id=ee2b052e395828a26759589f70157863&h=550',
              name:'Pure white water drop',
              descrition:'Pillow surface is uniform distribution ofultra soft massage',
              value: '72.0',
              promo: true
            },
            {
              img:'https://images.rtg-prod.com/accent-cabinets_room-grid.jpg?cache-id=ee2b052e395828a26759589f70157863&h=550',
              name:'Pure white water drop',
              descrition:'Pillow surface is uniform distribution ofultra soft massage',
              value: '72.0',
              promo: true
            },
            {
              img:'https://images.rtg-prod.com/accent-cabinets_room-grid.jpg?cache-id=ee2b052e395828a26759589f70157863&h=550',
              name:'Pure white water drop',
              descrition:'Pillow surface is uniform distribution ofultra soft massage',
              value: '72.0',
              promo: true
            },
          ]
        }
    }
    static navigationOptions = ({navigation}) => {
        return {
          tabBarIcon: ({tintColor}) =>{
          return tintColor === '#5a7391' ? <Icon name={'user'} color={tintColor} size={20} solid />
          : <Icon name={'user'} color={tintColor} size={20} Regular  />
        }}
      };
    render() {
        const { productsOption } = this.state
        return(
            <ScrollView contentContainerStyle={contenedor.contBrand}>
              <View style={contenedor.contLike}>
                {productsOption.map((data, i) => <ProductsOption key={i} {...data}/> )}
              </View>
            </ScrollView>
        );
    }
}
const mapStateToProps = (state) => {
    return {
      };
  }
  
  const mapDispatchToProps = () => {
      return{  
      }
    };
  
  const Brand = connect(
    mapStateToProps,
    mapDispatchToProps()
  )(brand)
  
  export default Brand 