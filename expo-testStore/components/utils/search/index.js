import React from 'react';
import { connect } from 'react-redux';
import { View, TextInput  } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { search } from '../../style/search';
import { contenedor } from '../../style/contenedor';
import { color } from '../../style/color';

class searchInput extends React.Component {
    constructor(props){
        super(props)
    }
    render() {
        const { tintColor } = this.props;
        return(
            <View style={contenedor.contHeader}>
                <View style={{...search.contSearch, backgroundColor: tintColor?color.whileTwo:color.whileTrasn }}>
                    <Icon name={'search'} color={tintColor||'#fff'} size={20} light />
                    <TextInput style={search.input} />
                </View>
                <Icon name={'comment-dots'} color={tintColor||'#fff'} size={20} light />
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
      };
  }
  
  const mapDispatchToProps = () => {
      return{  
      }
    };
  
  const Search = connect(
    mapStateToProps,
    mapDispatchToProps()
  )(searchInput)
  
  export default Search 