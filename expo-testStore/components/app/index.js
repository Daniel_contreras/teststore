import {createMaterialBottomTabNavigator} from "react-navigation-material-bottom-tabs";
import { createStackNavigator } from 'react-navigation-stack';
import {BorderlessButton} from 'react-native-gesture-handler';

import React from 'react'
import Icon from 'react-native-vector-icons/FontAwesome';

import Home from '../home'
import Store from '../store'
import Stopcart from '../stopcart'
import My from '../my'

const materialTabs = createMaterialBottomTabNavigator({
  Home: {
    screen: Home
  },
  Store: {
    screen: Store
  },
  Stopcart: {
    screen: Stopcart
  },
  My: {
    screen: My
  }
}, {
  initialRouteName: 'Home',
  activeColor: '#5a7391',
  inactiveColor: '#adb9c9',
  shifting: false,
  labeled: true,
  barStyle: {
    backgroundColor: '#fff',
    paddingTop: 0,
    height: 60,
    borderTopWidth: 1,
    borderTopColor: "#e5e5e5"
  }
});

export default materialTabs;
