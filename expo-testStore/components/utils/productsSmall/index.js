import React from 'react';
import { connect } from 'react-redux';
import { View, Text, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { productSmall } from '../../style/productSmall';
import { contenedor } from '../../style/contenedor';
import { color } from '../../style/color';

class productsSmall extends React.Component {
    constructor(props){
        super(props)
    }
    render() {
        const { img, name, value, tintColor } = this.props;
        return(
            <View style={contenedor.productSmall}>
                <Image style={productSmall.img} source={{uri:img}} resizeMode={'contain'}  />
                <View style={productSmall.cont}>
                    <Text style={productSmall.name} >{`${name.substr(0, 20)}${name.length > 20 ? '...' : '' }`}</Text>
                    <Text style={productSmall.value}>
                        <Icon name={'dollar-sign'} color={tintColor|| color.yellow} size={15} light />
                        {value}
                    </Text>
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
      };
  }
  
  const mapDispatchToProps = () => {
      return{  
      }
    };
  
  const ProductsSmall = connect(
    mapStateToProps,
    mapDispatchToProps()
  )(productsSmall)
  
  export default ProductsSmall 