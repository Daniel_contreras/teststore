import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger'

import Reducers from './reducers';
import TestStore  from './components';

const middleware = [thunk];
const store = createStore( Reducers,{ main: {} }, applyMiddleware(...middleware))

export default function App() {
  return (
    <Provider store={store}>
      <TestStore />
    </Provider>
  );
}
