import { StyleSheet } from 'react-native';
import { color } from './color'

export const product = StyleSheet.create({
    cont:{
        width: '57%',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    img:{
        width: '40%',
        height: 100,
    },
    name: {
        width: '100%',
        color: color.blue,
        fontSize: 17,
        marginBottom: 8,
        fontWeight: 'bold'
    },
    descrition: {
        width: '100%',
        color: color.blue,
        fontSize: 10,
        marginBottom: 5,
    },
    value: {
        marginTop: 5,
        width: '70%',
        color: color.yellow,
        fontSize: 15
    },
    contPromo:{
        backgroundColor: color.yellow,
        width: 50,
        height: 50,
        borderRadius: 50,
        position: 'absolute',
        top: -15,
        right: -15,
        zIndex: 2
    },

    textPromo:{
        position: 'absolute',
        color: color.while,
        fontWeight: 'bold',
        transform: [
            { translateY: 22 },
            { translateX: 5 },
            { rotate: '45deg' }
        ]
    }
});