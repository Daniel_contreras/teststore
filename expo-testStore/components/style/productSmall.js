import { StyleSheet } from 'react-native';
import { color } from './color'

export const productSmall = StyleSheet.create({
    cont:{
        width: '53%',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    img:{
        width: '45%',
        height: 100,
    },
    name: {
        width: '100%',
        color: color.blue,
        fontSize: 17,
        marginBottom: 8,
        fontWeight: 'bold'
    },
    value: {
        width: '100%',
        color: color.yellow,
        fontSize: 15
    }
});