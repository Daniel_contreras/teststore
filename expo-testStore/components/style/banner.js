import { StyleSheet } from 'react-native';
import { color } from './color'

export const banner = StyleSheet.create({

  primary: {
    width: '100%',
    height: 280,
    flexDirection: 'row',
    flexWrap:'wrap',
    justifyContent: 'center'
  },

  capa: {
    width: '100%',
    height: 280,
    backgroundColor: color.blueTrasn,
    position: 'absolute',
    zIndex: 1 
  },

  contLine:{
    width: '100%',
    marginTop: 30,
    flexDirection: 'row',
    justifyContent:'center',
    alignItems:'flex-end',
    zIndex:2
  },

  line:{
    width: '30%',
    height: 2,
    backgroundColor: color.while
  },
    
  text:{
    width: '30%',
    textAlign:'center',
    fontSize: 15,
    color: color.while,
    position: 'relative',
    top: 10
  },
  
  contPrimary:{
    width: '90%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent:'center',
    zIndex:2,
    borderWidth: 2,
    borderColor: color.while,
    borderTopWidth: 0,
    padding: 20
  },

  title: {
    width: '100%',
    fontSize: 30,
    color: color.while,
    textAlign: 'center',
    fontWeight: 'bold'
  },

  year:{
    width: '100%',
    textAlign:'center',
    fontSize: 15,
    color: color.while,
    marginTop: 5,
    zIndex: 2
  },

  contImg:{
    marginTop: 30,
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    zIndex: 2
  },

  imgUser:{
    width: 120,
    height: 120,
    borderRadius: 60,
    borderWidth: 2,
    borderColor: color.while
  },

  cameraUser:{
    width: 30,
    height: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color.yellow,
    borderRadius: 50,
    position: 'relative',
    marginTop: 85,
    right: 25
  },

  textUser: {
    width: '100%',
    color: color.while,
    fontSize: 22,
    fontWeight:'bold',
    textAlign: 'center'
  },

  contRegister:{
    width: '100%',
    justifyContent:'space-between',
    flexDirection:'row'
  },

  register:{
    width:'28%',
    flexDirection:'row',
    flexWrap:'wrap',
    justifyContent: 'center',
    zIndex: 2
  },

  registerValue: {
    width: '100%',
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    color: color.while
  },
  
  registerType: {
    textAlign: 'center',
    fontSize: 15,
    color: color.while
  }

});