import { StyleSheet } from 'react-native';
import { color } from './color';

export const contenedor = StyleSheet.create({

  cont: {
    width: '100%',
    //minHeight: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center' 
  },

  contHeader: {
    width: '90%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginLeft: '5%',
    marginRight: '5%',
    marginTop: 30,
    zIndex: 2
  },
  
  contMore: {
    width: '90%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginLeft: '5%',
    marginRight: '5%',
    marginTop: 10
  },

  productSmall: {
    width:'48%',
    padding: 5,
    backgroundColor: color.while,
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderRadius: 6,
    elevation: 3,
    marginBottom: 10
  },

  product: {
    width:'100%',
    padding: 15,
    backgroundColor: color.while,
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderRadius: 6,
    elevation: 3,
    marginBottom: 10,
    overflow: 'hidden'
  },

  contProductOption: {
    width:'100%',
    backgroundColor: color.while,
    justifyContent: 'space-between',
    flexDirection: 'row',
    flexWrap: 'wrap',
    borderRadius: 6,
    elevation: 3,
    marginBottom: 10,
    overflow: 'hidden'
  },

  productOption: {
    width:'100%',
    padding: 15,
    justifyContent: 'space-between',
    flexDirection: 'row',
    overflow: 'hidden'
  },

  contButton: {
    borderTopWidth: 1,
    borderTopColor: color.blueInative,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5
  },

  productLike: {
    width:'100%',
    backgroundColor: color.while,
    justifyContent: 'space-between',
    flexDirection: 'row',
    flexWrap: 'wrap',
    borderRadius: 6,
    elevation: 3,
    marginBottom: 10,
    overflow: 'hidden'
  },

  contTad: {
    width: '100%',
    minHeight: '95%',
    backgroundColor: color.whileTwo
  },

  contBrand: {
    width: '100%',
    paddingTop: 10,
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap'  
  },

  contSlider:{
    width: '100%',
    height: 220
  },

  contUser: {
    padding: 5
  },

  user: {
    width:'100%',
    padding: 15,
    backgroundColor: color.while,
    justifyContent: 'space-between',
    flexDirection: 'row',
    flexWrap: 'wrap',
    borderRadius: 6,
    elevation: 3,
    marginBottom: 10,
    height: 215,
    overflow: 'hidden'
  },

  contLike: {
    width: '90%',
    marginTop: 10,
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap' 
  }

});