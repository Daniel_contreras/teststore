import { StyleSheet } from 'react-native';
import { color } from './color'

export const productLike = StyleSheet.create({
    cont:{
        width: '100%',
        padding: 15,
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    img:{
        width: '100%',
        height: 150,
    },
    name: {
        width: '100%',
        color: color.blue,
        fontSize: 17,
        marginBottom: 8,
        fontWeight: 'bold'
    },
    descrition: {
        width: '100%',
        color: color.blue,
        fontSize: 10,
        marginBottom: 5,
    },
    value: {
        marginTop: 5,
        width: '70%',
        color: color.yellow,
        fontSize: 15
    },
    icon: {
        width: 55,
        height: 55,
        borderRadius: 50,
        margin: 5,
        backgroundColor: color.yellow
    },
    contInfo:{
        width: '55%',
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    contButton:{
        width: '20%',
        flexDirection: 'row',
        flexWrap: 'wrap'

    }
});