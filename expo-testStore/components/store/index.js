import React from 'react';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { View, Text, StatusBar , Dimensions, AsyncStorage } from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';
 
import Search from '../utils/search';
import Brand from './brand';

import { color } from '../style/color';
import { contenedor } from '../style/contenedor';
import { tab } from '../style/tab';

class store extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            index: 0,
            routes: [
              {
                key: 'brand',
                title: 'brand'
              }, {
                key: 'personal',
                title: 'personal'
              }, {
                key: 'shops',
                title: 'shops'
              }, {
                key: 'fitting',
                title: 'fitting'
              }
            ],
            refreshing: false

        }
    }
    static navigationOptions = ({navigation}) => {
        return {
          tabBarIcon: ({tintColor}) =>{
          return tintColor === '#5a7391' ? <Icon name={'store'} color={tintColor} size={20} solid />
          : <Icon name={'store'} color={tintColor} size={20} Regular  />
          },
      }
      };

      renderTabBar = props => {

        return <TabBar {...props} indicatorStyle={{
            backgroundColor: color.blue
          }} activeColor={color.blue} inactiveColor={color.blueInative} style={{
            backgroundColor: color.while,
            textTransform: 'capitalize'
          }} renderLabel={({route, focused, color}) => (<Text style={[tab.tabLabel, {
                color
              }]}>
            <Icon name={route.icon} size={15} color={color}/>
            &nbsp; {route.title}
          </Text>)}/>
      }
      
      renderScene = ({route}) => {
        const {navigation} = this.props
        switch (route.key) {
            case 'brand':
                return <Brand />;
            case 'personal':
                return <Brand />;
            case 'shops':
                return <Brand />;
            case 'fitting':
                return <Brand />;
            default:
                return null;
            }
      }

    render() {
        const {navigation} = this.props
        return(
            <View>
                <Search tintColor={color.blue} />
                <StatusBar barStyle="dark-content" />
                <View style={contenedor.contTad} >
                    <TabView
                        navigationState={this.state}
                        renderScene={this.renderScene}
                        onIndexChange={index => this.setState({ index })}
                        initialLayout={{ width: Dimensions.get('window').width }}
                        renderTabBar={this.renderTabBar}
                    />
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
      };
  }
  
  const mapDispatchToProps = () => {
      return{  
      }
    };
  
  const Store = connect(
    mapStateToProps,
    mapDispatchToProps()
  )(store)
  
  export default Store 