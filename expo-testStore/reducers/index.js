/* deoendencies */
import { combineReducers } from 'redux'

/*reducer into*/
import session from './session'

/* const */
const main = combineReducers({ session });
const _allReducer = combineReducers({main});

/* export default */
export default _allReducer;
