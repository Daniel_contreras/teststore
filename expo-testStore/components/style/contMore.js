import { StyleSheet } from 'react-native';
import { color } from './color'

export const contMore = StyleSheet.create({
    title:{
        color: color.blue,
        fontSize: 15
    },
    cont: {
        width: '40%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    info: {
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        paddingTop: 15
    }
});