import React from 'react';
import { connect } from 'react-redux';
import { View, Text, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { button } from '../../style/button';
import { user } from '../../style/user';
import { contenedor } from '../../style/contenedor';
import { color } from '../../style/color';

class userCart extends React.Component {
    constructor(props){
        super(props)
    }
    render() {
        const { img, name, descrition, value, premiun, tintColor } = this.props;
        return(
            <View style={contenedor.contUser}>
                <View style={contenedor.user}>
                    {premiun ?<View style={user.contPromo}>
                        <Text style={user.textPromo}>PR</Text>
                    </View> : <Text/>}
                    <View style={user.contImg}>
                        <Image style={user.img} source={{uri:img}} resizeMode={'cover'}  />
                    </View>
                    <View style={user.cont}>
                        <Text style={user.name} >{`${name.substr(0, 50)}${name.length > 50 ? '...' : '' }`}</Text>
                        <Text style={user.descrition} >{`${descrition.substr(0, 200)}${descrition.length > 200 ? '...' : '' }`}</Text>
                        
                        <Text style={button.primary}>view</Text>
                    </View>
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
      };
  }
  
  const mapDispatchToProps = () => {
      return{  
      }
    };
  
  const UserCart = connect(
    mapStateToProps,
    mapDispatchToProps()
  )(userCart)
  
  export default UserCart 