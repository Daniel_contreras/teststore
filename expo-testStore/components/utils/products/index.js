import React from 'react';
import { connect } from 'react-redux';
import { View, Text, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { button } from '../../style/button';
import { product } from '../../style/product';
import { contenedor } from '../../style/contenedor';
import { color } from '../../style/color';

class products extends React.Component {
    constructor(props){
        super(props)
    }
    render() {
        const { img, name, descrition, value, promo, tintColor } = this.props;
        return(
            <View style={contenedor.product}>
                {promo ?<View style={product.contPromo}>
                    <Text style={product.textPromo}>HOT</Text>
                </View> : <Text/>}
                <Image style={product.img} source={{uri:img}} resizeMode={'contain'}  />
                <View style={product.cont}>
                    <Text style={product.name} >{`${name.substr(0, 50)}${name.length > 50 ? '...' : '' }`}</Text>
                    <Text style={product.descrition} >{`${descrition.substr(0, 200)}${descrition.length > 200 ? '...' : '' }`}</Text>
                    <Text style={product.value}>
                        <Icon name={'dollar-sign'} color={tintColor|| color.yellow} size={15} light />
                        {value}
                    </Text>
                    <Text style={button.primary}>BUY</Text>
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
      };
  }
  
  const mapDispatchToProps = () => {
      return{  
      }
    };
  
  const Products = connect(
    mapStateToProps,
    mapDispatchToProps()
  )(products)
  
  export default Products 