import React from 'react';
import { connect } from 'react-redux';
import { View, Text, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { button } from '../../style/button';
import { product } from '../../style/product';
import { contenedor } from '../../style/contenedor';
import { color } from '../../style/color';

class products extends React.Component {
    constructor(props){
        super(props)
        this.state={
            buttons: [
                {
                    name:'Review',
                    icon: 'comment-alt'
                },
                {
                    name:'Logistics',
                    icon: 'box'
                },
                {
                    name:'More',
                    icon: 'ellipsis-h'
                }
            ]
        }
    }
    render() {
        const { img, name, descrition, value, tintColor } = this.props;
        const { buttons } = this.state;
        return(
            <View style={contenedor.contProductOption}>
                <View style={contenedor.productOption}>
                    <Image style={product.img} source={{uri:img}} resizeMode={'contain'}  />
                    <View style={product.cont}>
                        <Text style={product.name} >{`${name.substr(0, 50)}${name.length > 50 ? '...' : '' }`}</Text>
                        <Text style={product.descrition} >{`${descrition.substr(0, 200)}${descrition.length > 200 ? '...' : '' }`}</Text>
                        <Text style={product.value}>
                            <Icon name={'dollar-sign'} color={tintColor|| color.yellow} size={15} light />
                            {value}
                        </Text>
                    </View>
                </View>
                <View style={contenedor.contButton}>
                    {buttons.map((data, i)=> <Text key={i} style={{...button.default, borderLeftWidth: i>0 ? 1 : 0}} >
                        <Icon name={data.icon} color={color.blue} size={15} light /> {data.name}</Text>)}
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
      };
  }
  
  const mapDispatchToProps = () => {
      return{  
      }
    };
  
  const Products = connect(
    mapStateToProps,
    mapDispatchToProps()
  )(products)
  
  export default Products 