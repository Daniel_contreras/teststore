import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Carousel from 'react-native-snap-carousel';
import { connect } from 'react-redux';
import { View, ScrollView } from 'react-native';

import UserCart from '../../utils/userCart';
import ProductsLike from '../../utils/productsLike';
import { contenedor } from '../../style/contenedor';
import { sliderWidth, itemWidth } from '../../style/slider';


class brand extends React.Component {
    constructor(props){
        super(props)
        this.state = {
          users:[
            {
              img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQNkkyvyMtZfHxP3FiObw81pAVx6U-SYHOpdzcEo9Z2W8oCxPspmw',
              name: 'Elegant',
              descrition: 'Star Designer',
              premiun: true
            },
            {
              img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQNkkyvyMtZfHxP3FiObw81pAVx6U-SYHOpdzcEo9Z2W8oCxPspmw',
              name: 'Marclano',
              descrition: 'Classic heritage',
              premiun: false
            },
            {
              img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQNkkyvyMtZfHxP3FiObw81pAVx6U-SYHOpdzcEo9Z2W8oCxPspmw',
              name: 'Zucalli',
              descrition: 'Tile collocation',
              premiun: false
            },
          ],
          like:[
            {
              img: 'https://cursodeorganizaciondelhogar.com/wp-content/uploads/2017/08/decoracion-de-interiores-salas-modernas-5.jpg',
              icon: 'https://daemon4.com/wp-content/uploads/Daemon4-Icono-Expo_de_Muebles.png',
              name: 'Synergla Tech',
              descrition: 'Give a person with a kind of warm bold and unrestrain',
              promo: true,
              value: 120
            },
            {
              img: 'https://cursodeorganizaciondelhogar.com/wp-content/uploads/2017/08/decoracion-de-interiores-salas-modernas-5.jpg',
              icon: 'https://daemon4.com/wp-content/uploads/Daemon4-Icono-Expo_de_Muebles.png',
              name: 'Synergla Tech',
              descrition: 'Give a person with a kind of warm bold and unrestrain',
              promo: false,
              value: 120
            },
            {
              img: 'https://cursodeorganizaciondelhogar.com/wp-content/uploads/2017/08/decoracion-de-interiores-salas-modernas-5.jpg',
              icon: 'https://daemon4.com/wp-content/uploads/Daemon4-Icono-Expo_de_Muebles.png',
              name: 'Synergla Tech',
              descrition: 'Give a person with a kind of warm bold and unrestrain',
              promo: true,
              value: 120
            },
          ]
        }
    }
    static navigationOptions = ({navigation}) => {
        return {
          tabBarIcon: ({tintColor}) =>{
          return tintColor === '#5a7391' ? <Icon name={'user'} color={tintColor} size={20} solid />
          : <Icon name={'user'} color={tintColor} size={20} Regular  />
        }}
      };
    render() {
        const { users, like } = this.state
        return(
            <ScrollView contentContainerStyle={contenedor.contBrand}>
              <View style={contenedor.contSlider}>
                <Carousel
                  data={users}
                  renderItem={({item, index}) => <UserCart key={index} {...item} />}
                  sliderWidth={sliderWidth}
                  itemWidth={itemWidth}
                  layout={'default'}
                  inactiveSlideScale={1}
                  inactiveSlideOpacity={1}
                  activeSlideAlignment='start'
                  sliderHeight={220}
                />
              </View>
              <View style={contenedor.contLike}>
                {like.map((data, i) => <ProductsLike key={i} {...data}/> )}
              </View>
            </ScrollView>
        );
    }
}
const mapStateToProps = (state) => {
    return {
      };
  }
  
  const mapDispatchToProps = () => {
      return{  
      }
    };
  
  const Brand = connect(
    mapStateToProps,
    mapDispatchToProps()
  )(brand)
  
  export default Brand 