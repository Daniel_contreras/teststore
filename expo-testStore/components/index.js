import React from 'react';
import { connect } from 'react-redux'
import { createAppContainer } from "react-navigation";
import { fromRight } from 'react-navigation-transitions';
import { createStackNavigator } from 'react-navigation-stack';
import {TouchableOpacity, Text, View, Image} from 'react-native';

import MiApp from './app';
import Search from './utils/search';

const AppNavigator = createStackNavigator({
    MiApp: { screen: MiApp },
  },{
    initialRouteName: 'MiApp',
    transitionConfig: () => fromRight(),
    defaultNavigationOptions: {
      headerTransparent: true,
      // headerTitle: () => (<Search />),
    },
  });

const AppContainer = createAppContainer(AppNavigator);

class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}
const mapStateToProps = (state) => {
    return {
      auth: state.main.session.auth,
    };
  }

  const mapDispatchToProps = () => {
      return{
      }
    };

  const app = connect(
    mapStateToProps,
    mapDispatchToProps()
  )(App)

  export default app
