import React from 'react';
import { connect } from 'react-redux';
import { View, Text, ActivityIndicator, ImageBackground, AsyncStorage } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { styles } from './style';

class stopcart extends React.Component {
    constructor(props){
        super(props)
    }
    static navigationOptions = ({navigation}) => {
        return {
          tabBarIcon: ({tintColor}) =>{
          return tintColor === '#5a7391' ? <Icon name={'shopping-cart'} color={tintColor} size={20} solid />
          : <Icon name={'shopping-cart'} color={tintColor} size={20} Regular  />
        }}
      };
    render() {
        return(
            <View style={{  flex: 1, justifyContent: "center", alignItems: "center" }}>
                <Text>text Stopcart</Text>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
      };
  }
  
  const mapDispatchToProps = () => {
      return{  
      }
    };
  
  const Stopcart = connect(
    mapStateToProps,
    mapDispatchToProps()
  )(stopcart)
  
  export default Stopcart 