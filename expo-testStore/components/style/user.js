import { StyleSheet } from 'react-native';
import { color } from './color'

export const user = StyleSheet.create({
    cont:{
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
    },
    contImg:{
        width: '100%',
        marginBottom: 20,
        justifyContent: 'center',
        display:'flex',
        flexDirection: 'row'
    },
    img:{
        width: 80,
        height: 80,
        borderRadius: 50
    },
    name: {
        width: '100%',
        color: color.blue,
        fontSize: 17,
        marginBottom: 8,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    descrition: {
        width: '100%',
        color: color.blue,
        fontSize: 15,
        marginBottom: 5,
        textAlign: 'center'
    },
    contPromo:{
        backgroundColor: color.yellow,
        width: 50,
        height: 50,
        borderRadius: 50,
        position: 'absolute',
        top: -15,
        right: -15
    },

    textPromo:{
        position: 'absolute',
        color: color.while,
        fontWeight: 'bold',
        transform: [
            { translateY: 22 },
            { translateX: 5 },
            { rotate: '45deg' }
        ]
    }
});