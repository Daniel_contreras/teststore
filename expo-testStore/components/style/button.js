import { StyleSheet } from 'react-native';
import { color } from './color'

export const button = StyleSheet.create({
    primary:{
        color: color.while,
        fontSize: 15,
        fontWeight: 'bold',
        backgroundColor: color.blue,
        padding: 2,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 15
    },

    default:{
        color: color.blue,
        fontSize: 15,
        fontWeight: 'bold',
        backgroundColor: color.while,
        padding: 5,
        paddingLeft: 10,
        paddingRight: 10,
        borderColor: color.blueInative,
        width:'33%',
        textAlign: 'justify'
    }
});