import { StyleSheet } from 'react-native';
import { color } from './color'

export const search = StyleSheet.create({
    contSearch:{
        width: '80%',
        height: 30,
        backgroundColor: color.whileTrasn,
        borderRadius: 6,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        padding: 5
    },
    input: {
        width: '90%',
        paddingLeft: 5,
        color: color.while
    }
});