import React from 'react';
import { connect } from 'react-redux';
import { View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { contMore } from '../../style/contMore';
import { contenedor } from '../../style/contenedor';
import { color } from '../../style/color';

class containerMore extends React.Component {
    constructor(props){
        super(props)
    }
    render() {
        const { title, children, tintColor } = this.props;
        return(
            <View style={contenedor.contMore}>
                <Text style={contMore.title} >{title}</Text>
                <View style={contMore.cont}>
                    <Text style={contMore.title} >more </Text>
                    <Icon name={'chevron-right'} color={tintColor|| color.blue} size={15} light />
                </View>
                <View style={contMore.info}>
                    {children}
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
      };
  }
  
  const mapDispatchToProps = () => {
      return{  
      }
    };
  
  const ContainerMore = connect(
    mapStateToProps,
    mapDispatchToProps()
  )(containerMore)
  
  export default ContainerMore 