import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { connect } from 'react-redux';
import { View, Text, ScrollView, ImageBackground, StatusBar, Image, Dimensions } from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';

import Bought from './bought';

import { banner }  from '../style/banner';
import { contenedor }  from '../style/contenedor';
import { color } from '../style/color';
import { tab } from '../style/tab';

class myClass extends React.Component {
    constructor(props){
        super(props)
        this.state = {
          img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQNkkyvyMtZfHxP3FiObw81pAVx6U-SYHOpdzcEo9Z2W8oCxPspmw',        
          register:[
            {
              value: '0.95K',
              type: 'Spend'
            },
            {
              value: '14',
              type: 'Already buy'
            },
            {
              value: '68',
              type: 'Collecion'
            },
          ],
          index: 0,
          routes: [
            {
              key: 'bought',
              title: 'bought'
            }, {
              key: 'peding',
              title: 'peding'
            }, {
              key: 'unpaid',
              title: 'unpaid'
            },
          ],
          refreshing: false
          
        }  
    }
    static navigationOptions = ({navigation}) => {
        return {
          tabBarIcon: ({tintColor}) =>{
          return tintColor === '#5a7391' ? <Icon name={'user'} color={tintColor} size={20} solid />
          : <Icon name={'user'} color={tintColor} size={20} Regular  />
        }}
    };

    renderTabBar = props => {
      return <TabBar {...props} indicatorStyle={{
          backgroundColor: color.blue
        }} activeColor={color.blue} inactiveColor={color.blueInative} style={{
          backgroundColor: color.while,
          textTransform: 'capitalize'
        }} renderLabel={({route, focused, color}) => (<Text style={[tab.tabLabel, {
              color
            }]}>
          <Icon name={route.icon} size={15} color={color}/>
          &nbsp; {route.title}
        </Text>)}/>
    }
      
    renderScene = ({route}) => {
      const {navigation} = this.props
      switch (route.key) {
          case 'bought':
              return <Bought />;
          case 'peding':
              return <Bought />;
          case 'unpaid':
              return <Bought />;
          default:
              return null;
          }
    }  
    render() {
        const { img, register } = this.state
        return(
          <ScrollView contentContainerStyle={contenedor.cont}>
            <StatusBar barStyle="dark-content" />
            <View style={banner.capa}></View>
            <ImageBackground source={{uri:'https://estaticos.sancarlos.es/ProductImages/001/052566251501.jpg'}} style={banner.primary} >
              <View style={contenedor.contHeader}>
                <Icon name={'cog'} color={color.while} size={20} light />
                <Text style={{...banner.text, width:'50%', fontWeight: 'bold'}}>My information</Text>
                <Icon name={'comment-dots'} color={color.while} size={20} light />
              </View>
              <View style={banner.contImg}>
                  <Image style={banner.imgUser} source={{uri:img}} resizeMode={'cover'}  /> 
                  <View style={banner.cameraUser}>
                    <Text><Icon name={'camera'} color={color.while} size={20} light /></Text>                              
                  </View> 
                  <Text style={banner.textUser}>Owen Davey</Text>                              
              </View>
              <View style={banner.contRegister}>
                {register.map((data, i)=><View key={i} style={banner.register}>
                  <Text style={banner.registerValue} >{data.value}</Text>
                  <Text style={banner.registerType} >{data.type}</Text>
                </View>)}
              </View>
            </ImageBackground>
            <View style={contenedor.contTad} >
              <TabView
                  navigationState={this.state}
                  renderScene={this.renderScene}
                  onIndexChange={index => this.setState({ index })}
                  initialLayout={{ width: Dimensions.get('window').width }}
                  renderTabBar={this.renderTabBar}
              />
            </View>
          </ScrollView>
        );
    }
}
const mapStateToProps = (state) => {
    return {
      };
  }
  
  const mapDispatchToProps = () => {
      return{  
      }
    };
  
  const My = connect(
    mapStateToProps,
    mapDispatchToProps()
  )(myClass)
  
  export default My 