import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import moment from 'moment';

import { connect } from 'react-redux';
import { View, Text, ScrollView, ImageBackground, StatusBar  } from 'react-native';

import Search from '../utils/search';
import ContMore  from '../utils/contMore';
import ProductsSmall from '../utils/productsSmall';
import Products from '../utils/products';
import { banner }  from '../style/banner';
import { contenedor }  from '../style/contenedor';
import { color } from '../style/color';

class home extends React.Component {
    constructor(props){
        super(props)
        this.state = {
          newProducts :[
            {
              img:'https://d92336rahfxx8.cloudfront.net/images/detailed/2/4__3_.jpg?t=1561449054',
              name:'Aladdin and his Magical Oil Lamp for home decoration',
              value: 20.56
            },
            {
              img:'https://d92336rahfxx8.cloudfront.net/images/detailed/2/4__3_.jpg?t=1561449054',
              name:'Aladdin and his Magical Oil Lamp for home decoration',
              value: 20.56
            }
          ],
          theRecommended: [
            {
              img:'https://images.rtg-prod.com/accent-cabinets_room-grid.jpg?cache-id=ee2b052e395828a26759589f70157863&h=550',
              name:'Home Decor, Accessories & Accents',
              descrition:'Home Decor, Accessories & Accents',
              value: 20.56,
              promo: true
            },
            {
              img:'https://images.rtg-prod.com/accent-cabinets_room-grid.jpg?cache-id=ee2b052e395828a26759589f70157863&h=550',
              name:'Home Decor, Accessories & Accents',
              descrition:'Home Decor, Accessories & Accents',
              value: 20.56,
              promo: false
            },
            {
              img:'https://images.rtg-prod.com/accent-cabinets_room-grid.jpg?cache-id=ee2b052e395828a26759589f70157863&h=550',
              name:'Home Decor, Accessories & Accents',
              descrition:'Home Decor, Accessories & Accents',
              value: 20.56,
              promo: false
            }
          ]
        }
    }
    static navigationOptions = ({navigation}) => {
        return {
          tabBarIcon: ({tintColor}) =>{
          return tintColor === '#5a7391' ? <Icon name={'home'} color={tintColor} size={20} solid />
          : <Icon name={'home'} color={tintColor} size={20} Regular  />
        }}
      };
    render() {
        const { newProducts, theRecommended } = this.state
        return(
            <ScrollView contentContainerStyle={contenedor.cont}>
                <StatusBar barStyle="dark-content" />
                <View style={banner.capa}></View>
                <ImageBackground source={{uri:'https://estaticos.sancarlos.es/ProductImages/001/052566251501.jpg'}} style={banner.primary} >
                   <Search  />
                   <View style={banner.contLine}>
                     <Text style={banner.line} ></Text>
                     <Text style={banner.text} >BRADLEY</Text>
                     <Text style={banner.line} ></Text>
                   </View>
                   <View style={banner.contPrimary}>
                     <Text style={banner.title} >NEW</Text>
                     <Text style={banner.title} >COLLECTIONS</Text>
                   </View>
                   <Text style={banner.year} >• {moment().format('YYYY')} •</Text>
                </ImageBackground>
                <ContMore title='New Products'>
                    {newProducts.map( (data, i) => <ProductsSmall key={i} {...data} />)}
                </ContMore>
                <ContMore title='The recommended'>
                    {theRecommended.map( (data, i) => <Products key={i} {...data} />)}
                </ContMore>
            </ScrollView>
        );
    }
}
const mapStateToProps = (state) => {
    return {
      };
  }
  
  const mapDispatchToProps = () => {
      return{  
      }
    };
  
  const Home = connect(
    mapStateToProps,
    mapDispatchToProps()
  )(home)
  
  export default Home 